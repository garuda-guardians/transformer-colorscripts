#!/bin/sh

# A basic install script for transformer-colorscripts

INSTALL_DIR='/usr/local/opt'
BIN_DIR='/usr/local/bin'

# deleting directory if it already exists
rm -rf "$INSTALL_DIR/transformer-colorscripts" || return 1

# making the necessary folder structure
mkdir -p "$INSTALL_DIR/transformer-colorscripts" || return 1

# moving all the files to appropriate locations
cp -rf colorscripts "$INSTALL_DIR/transformer-colorscripts"
cp transformer-colorscripts.py "$INSTALL_DIR/transformer-colorscripts"
cp transformer.json "$INSTALL_DIR/transformer-colorscripts"

# create symlink in usr/bin
rm -rf "$BIN_DIR/transformer-colorscripts" || return 1
ln -s "$INSTALL_DIR/transformer-colorscripts/transformer-colorscripts.py" "$BIN_DIR/transformer-colorscripts"
