# Transformers colorscripts

A script to print out images of Transformers to the terminal. Inspired by
[DT's colorscripts compilation](https://gitlab.com/dwt1/shell-color-scripts)

## Table of contents
[[_TOC_]]

## Description
Prints out colored unicode sprites of Transformers onto your terminal. Includes various Transformers across different series and generations.

## Visuals
### Demo GIFs
Demo of the program being used

![demo of program in action](./demo_images/transformers-colorscripts-demo.gif)

Demo of the program running on terminal startup.

![demo of random Transformers on terminal spawn](./demo_images/terminal-startup-demo.gif)

### Screenshots
![screenshot](./demo_images/transformers-colorscripts-screenshot-1.png)
![screenshot](./demo_images/transformers-colorscripts-screenshot-2.png)
![screenshot](./demo_images/transformers-colorscripts-screenshot-3.png)

## Requirements
The program requires `python3` to run and a terminal with true color support,
which most modern terminals have. More on terminals and color support can be found in
[this gist](https://gist.github.com/XVilka/8346728)

## Installation

### On Arch or Arch based distros
The utility is available as an AUR package
[transformers-colorscripts-git](https://aur.archlinux.org/packages/transformers-colorscripts-git).
You can install this manually or using an AUR helper. For manual installation,
download the PKGBUILD file from this repository. Then run
'''
makepkg -cf
''''
which will create a .pkg.tar.zst file. Then run
'''
sudo pacman -U <filename.pkg.tar.zst>
'''

Or you can use an AUR Helper such as yay and run
'''
yay -S transformers-colorscripts-git
'''
or
'''
paru -S transformers-colorscripts-git
'''


### On other distros and MacOS
Clone or download the repository

git clone https://gitlab.com/garuda-guardians/transformer-colorscripts.git
'''
`cd` into the directory and run the install script
'''
cd transformers-colorscripts
sudo ./install.sh
'''
Now the program should be installed. You can check this by running
'''
transformers-colorscripts

'''
Which should print out the help page for the program

### Uninstall
To uninstall the program you can run the uninstall script. Make the script
executable if it already isn't
'''
chmod +x ./uninstall.sh
'''
and run the script
'''
sudo ./uninstall.sh
'''

## Usage
You can run the program from the command line to either display a Transformer of your
choice by specifying the Transformer name or make it display a random Transformer.
'''

usage: transformers-colorscripts [OPTION] [TRANSFORMER NAME]

CLI utility to print out unicode image of a Transformer in your shell

optional arguments:
-h, --help Show this help message and exit
-l, --list Print list of all Transformers
-n NAME, --name NAME Select Transformer by name.
-f FORM, --form FORM Show an alternate form of a Transformer
--no-title Do not display Transformer name
-s, --shiny Show the shiny version of the Transformer instead
-b, --big Show a larger version of the sprite
-r [RANDOM], --random [RANDOM]
Show a random Transformer.

'''

Example of printing out a specific Transformer

'''
transformers-colorscripts -n Optimus Prime
'''

Example of printing out a specific shiny Transformer

'''
transformers-colorscripts -n Bumblebee -s
'''
Example of printing out a random Transformer

'''
transformers-colorscripts -r
'''

Example of printing out a random transformer from generation 1
'''
transformer-colorscripts -r 1
'''
Example of printing out a random transformer from generation 1-3
'''
transformer-colorscripts -r 1-3
'''
Example of printing out a larger sprite
```
transformer-colorscripts -n Bumblebee -b
```
Example of printing out an alternate form
```
transformer-colorscripts -n deoxys --form defense
```
Some transformer with spaces or periods or other special characters in their name
might not be spelled as expected, some examples include:
```
farfetch'd -> farfetchd
mr.mime -> mr-mime
```
These are rare exceptions, and if required you can parse the `--list` page to see
the names of all the transformer.

### Running on terminal startup
#### On Bash and ZSH
You can display a random transformer whenever a terminal gets launched by adding
the `transformer-colorscripts -r` command to your *.bashrc* or .*zshrc*.

#### On Fish
If you have fish as your user shell you can display a random transformer on terminal
startup by overriding the `fish_greeting` in your `config.fish`
```
function fish_greeting
     transformer-colorscripts -r
end
```
A more advaced setup combining multiple colorscripts can be found on transformer-colorscripts#2

## Location of the files
The program is located at usr/local/opt/transformers-colorscripts/ with the script being symlinked to /usr/local/bin/

## How it works
The program itself is a simple python script that prints out text files corresponding
to the relevant Transformers or a randomly selected Transformer. The sprites are simple text
files that use unicode characters and ANSI color codes to display images of Transformers.
The text sprite files can be found in the colorscripts folder. The files were generated
using sprites taken from [Transformers Sprite](https://gitlab.com/garuda-guardians/transformer-colorscripts.git) as the base and converted to unicode sprites using custom scripts that can be found in [this repo](https://gitlab.com/garuda-guardians/transformer-colorscripts.git)

## Similar projects
`transformers-colorscripts` is not the exclusive nor the first program to print out
Transformers sprites to the terminal. You can check out these really cool projects as
well

[transformer-get]
[transformershell]
[tformers]

An older more minimal shell script version of the project is now being maintained
as

[t-script]

a much more comprehensive comparison of the different projects can be found on
the [Transformershell Readme]

## Credits

All the Transformers designs, names, branding etc. are [transformer](https://transformers.fandom.com/wiki/Autobot)
The box art sprites were taken from the amazing [Transformers Sprite](https://www.iconfinder.com/search/icons?q=transformers)

## Author

Garuda Guardians
[https://gitlab.com/garuda-guardians](https://gitlab.com/garuda-guardians)

## License
The MIT License (MIT)

