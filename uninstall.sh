#! /bin/sh

# A basic script to uninstall transformer colorscripts

INSTALL_DIR='/usr/local/opt'
BIN_DIR='/usr/local/bin'

# Remove directories where files have been installed
rm -rf "$INSTALL_DIR/transformer-colorscripts"
rm -rf "$BIN_DIR/transformer-colorscripts"
